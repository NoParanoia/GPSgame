package com.noparanoia.gpsgame;


import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import static android.content.ContentValues.TAG;


public class MovieActivity extends Activity {

    Preferences prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_movies);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        prefs = new Preferences();
        Uri uri;

                final VideoView videoView = findViewById(R.id.videoView1);

        if(prefs.returnSharedPreferences(MovieActivity.this, "mov_start") && prefs.returnSharedPreferences(MovieActivity.this, "mov_mid") && !prefs.returnSharedPreferences(MovieActivity.this, "mov_end") ) {
           String uriPath = "android.resource://com.noparanoia.gpsgame/raw/uitlegspel_new";
            uri = Uri.parse(uriPath);
        } else if (prefs.returnSharedPreferences(MovieActivity.this, "mov_start") && prefs.returnSharedPreferences(MovieActivity.this, "mov_mid") && prefs.returnSharedPreferences(MovieActivity.this, "mov_end")) {
            String uriPath = "android.resource://com.noparanoia.gpsgame/raw/slot_scene";
            uri = Uri.parse(uriPath);
        } else {
            prefs.setSharedPreferences(MovieActivity.this, "mov_start", true);
            String uriPath = "android.resource://com.noparanoia.gpsgame/raw/hellokids_new";
            uri = Uri.parse(uriPath);
        }

        videoView.setVideoURI(uri);

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()  {
            @Override
                public void onPrepared(MediaPlayer mp) {
                    Log.i(TAG, "Duration = " +
                    videoView.getDuration());
            }
        });

        videoView.start();

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                Intent intent;
                intent = new Intent();
                if (prefs.returnSharedPreferences(MovieActivity.this, "mov_start") && !prefs.returnSharedPreferences(MovieActivity.this, "mov_mid")) {
                    intent = new Intent(MovieActivity.this, PictureActivity.class);
                } else if (prefs.returnSharedPreferences(MovieActivity.this, "mov_start") && prefs.returnSharedPreferences(MovieActivity.this, "mov_mid")) {
                    intent = new Intent(MovieActivity.this, GPSActivity.class);
                }
                startActivity(intent);

            }
        });
    }
}
