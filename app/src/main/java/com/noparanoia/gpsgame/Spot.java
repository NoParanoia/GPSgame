package com.noparanoia.gpsgame;

public class Spot {
    private String name;
    private double x;
    private double y;
    private Boolean found;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public double getX() {
        return x;
    }
    public void setX(double latitude) {
        this.x = latitude;
    }

    public double getY() {
        return y;
    }
    public void setY(double longitude) {
        this.y = longitude;
    }

    public boolean getFound() {
        return found;
    }
    public void setFound(boolean radius) {
        this.found = found;
    }
}