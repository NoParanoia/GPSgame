package com.noparanoia.gpsgame;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;




public class PictureActivity extends Activity implements View.OnTouchListener {

    private static double iSpotX1 = 113;
    private static double iSpotY1 = 245;
    private static double iSpotX2 = 270;
    private static double iSpotY2 = 196;
    private static double iSpotX3 = 260;
    private static double iSpotY3 = 396;
    private static double iSpotX4 = 378;
    private static double iSpotY4 = 50;
    private static double iSpotX5 = 482;
    private static double iSpotY5 = 270;
    private static double iSpotX6 = 496;
    private static double iSpotY6 = 407;

    private static int margin = 50;

    private static int radius = 40;

    private static double correction = 1.7;

    ImageView ivl;
    ImageView ivr;
    ImageView ivb1;
    ImageView ivb2;
    ImageView ivb3;
    ImageView ivb4;
    ImageView ivb5;
    ImageView ivb6;

    Button btnGo;

    Bitmap imageLeft;
    Bitmap imageRight;

    Canvas canvas;
    Paint paint;

    MediaPlayer mpP;

    Preferences prefs;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_picture);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ivl = findViewById(R.id.left_image);
        ivr = findViewById(R.id.right_image);
        ivb1 = findViewById(R.id.bottom_image1);
        ivb2 = findViewById(R.id.bottom_image2);
        ivb3 = findViewById(R.id.bottom_image3);
        ivb4 = findViewById(R.id.bottom_image4);
        ivb5 = findViewById(R.id.bottom_image5);
        ivb6 = findViewById(R.id.bottom_image6);

        btnGo = findViewById(R.id.GO);
        btnGo.setVisibility(View.INVISIBLE);

        imageLeft = BitmapFactory.decodeResource(getResources(), R.drawable.foto1zoekverschillen).copy(Bitmap.Config.ARGB_8888, true);
        imageLeft = BitmapFactory.decodeResource(getResources(), R.drawable.foto1zoekverschillen).copy(Bitmap.Config.ARGB_8888, true);
        ivl.setImageDrawable(new BitmapDrawable(imageLeft));

        imageRight = BitmapFactory.decodeResource(getResources(), R.drawable.foto2zoekverschillen).copy(Bitmap.Config.ARGB_8888, true);
        ivr.setImageBitmap(imageRight);

        prefs = new Preferences();
        // Initialize shared preferences
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot1") == null) {
            prefs.setSharedPreferences(PictureActivity.this, "pSpot1", false);
        } else if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot1") == false) {
            ivb1.setImageResource(R.drawable.inventory_theepot_cut_gray);
        } else {
            ivb1.setImageResource(R.drawable.inventory_theepot_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot2") == null) {
            prefs.setSharedPreferences(PictureActivity.this, "pSpot2", false);
        } else if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot2") == false) {
            ivb2.setImageResource(R.drawable.inventory_vis_cut_gray);
        } else {
            ivb2.setImageResource(R.drawable.inventory_vis_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot3") == null) {
            prefs.setSharedPreferences(PictureActivity.this, "pSpot3", false);
        } else if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot3") == false) {
            ivb3.setImageResource(R.drawable.inventory_winkelwagen_cut_gray);
        } else {
            ivb3.setImageResource(R.drawable.inventory_winkelwagen_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot4") == null) {
            prefs.setSharedPreferences(PictureActivity.this, "pSpot4", false);
        } else if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot4") == false) {
            ivb4.setImageResource(R.drawable.inventory_slang_cut_gray);
        } else {
            ivb4.setImageResource(R.drawable.inventory_slang_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot5") == null) {
            prefs.setSharedPreferences(PictureActivity.this, "pSpot5", false);
        } else if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot5") == false) {
            ivb5.setImageResource(R.drawable.inventory_megamindy_cut_gray);
        } else {
            ivb5.setImageResource(R.drawable.inventory_megamindy_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot6") == null) {
            prefs.setSharedPreferences(PictureActivity.this, "pSpot6", false);
        } else if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot4") == false) {
            ivb6.setImageResource(R.drawable.inventory_bal_cut_gray);
        } else {
            ivb6.setImageResource(R.drawable.inventory_bal_cut);
        }

        canvas = new Canvas(imageRight);
        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(6);

        checkPicture(0, 0);

        ivl.setOnTouchListener(this);
        ivr.setOnTouchListener(this);
    }

    public boolean checkPicture(double x, double y) {
        prefs = new Preferences();

        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot1") == false) {
            if (x > iSpotX1 - margin && x < iSpotX1 + margin && y > iSpotY1 - margin && y < iSpotY1 + margin) {
                prefs.setSharedPreferences(PictureActivity.this, "pSpot1", true);
                prefs.setSharedPreferences(PictureActivity.this, "audio_playing", true);
                ivb1.setImageResource(R.drawable.inventory_theepot_cut);
                canvas.drawCircle((int) (iSpotX1 * correction), (int) (iSpotY1 * correction), radius, paint);
                mpP = MediaPlayer.create(PictureActivity.this, R.raw.gevonden_item_theepot);
                mpP.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(PictureActivity.this, "audio_playing", false);
                    }
                });
                mpP.start();
            }
        } else {
            canvas.drawCircle((int) (iSpotX1 * correction), (int) (iSpotY1 * correction), radius, paint);
            ivb1.setImageResource(R.drawable.inventory_theepot_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot2") == false) {
            if (x > iSpotX2 - margin && x < iSpotX2 + margin && y > iSpotY2 - margin && y < iSpotY2 + margin) {
                prefs.setSharedPreferences(PictureActivity.this, "pSpot2", true);
                prefs.setSharedPreferences(PictureActivity.this, "audio_playing", true);
                ivb2.setImageResource(R.drawable.inventory_vis_cut);
                canvas.drawCircle((int) (iSpotX2 * correction), (int) (iSpotY2 * correction), radius, paint);
                mpP = MediaPlayer.create(PictureActivity.this, R.raw.gevonden_item_vis);
                mpP.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(PictureActivity.this, "audio_playing", false);
                    }
                });
                mpP.start();
            }
        } else {
            canvas.drawCircle((int) (iSpotX2 * correction), (int) (iSpotY2 * correction), radius, paint);
            ivb2.setImageResource(R.drawable.inventory_vis_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot3") == false) {
            if (x > iSpotX3 - margin && x < iSpotX3 + margin && y > iSpotY3 - margin && y < iSpotY3 + margin) {
                prefs.setSharedPreferences(PictureActivity.this, "pSpot3", true);
                prefs.setSharedPreferences(PictureActivity.this, "audio_playing", true);
                ivb3.setImageResource(R.drawable.inventory_winkelwagen_cut);
                canvas.drawCircle((int) (iSpotX3 * correction), (int) (iSpotY3 * correction), radius, paint);
                mpP = MediaPlayer.create(PictureActivity.this, R.raw.gevonden_item_winkelwagen);
                mpP.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(PictureActivity.this, "audio_playing", false);
                    }
                });
                mpP.start();
            }
        } else {
            canvas.drawCircle((int) (iSpotX3 * correction), (int) (iSpotY3 * correction), radius, paint);
            ivb3.setImageResource(R.drawable.inventory_winkelwagen_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot4") == false) {
            if (x > iSpotX4 - margin && x < iSpotX4 + margin && y > iSpotY4 - margin && y < iSpotY4 + margin) {
                prefs.setSharedPreferences(PictureActivity.this, "pSpot4", true);
                prefs.setSharedPreferences(PictureActivity.this, "audio_playing", true);
                ivb4.setImageResource(R.drawable.inventory_slang_cut);
                canvas.drawCircle((int) (iSpotX4 * correction), (int) (iSpotY4 * correction), radius, paint);
                mpP = MediaPlayer.create(PictureActivity.this, R.raw.gevonden_item_slang);
                mpP.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(PictureActivity.this, "audio_playing", false);
                    }
                });
                mpP.start();
            }
        } else {
            canvas.drawCircle((int) (iSpotX4 * correction), (int) (iSpotY4 * correction), radius, paint);
            ivb4.setImageResource(R.drawable.inventory_slang_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot5") == false) {
            if (x > iSpotX5 - margin && x < iSpotX5 + margin && y > iSpotY5 - margin && y < iSpotY5 + margin) {
                prefs.setSharedPreferences(PictureActivity.this, "pSpot5", true);
                prefs.setSharedPreferences(PictureActivity.this, "audio_playing", true);
                ivb5.setImageResource(R.drawable.inventory_megamindy_cut);
                canvas.drawCircle((int) (iSpotX5 * correction), (int) (iSpotY5 * correction), radius, paint);
                mpP = MediaPlayer.create(PictureActivity.this, R.raw.gevonden_item_megamindy);
                mpP.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(PictureActivity.this, "audio_playing", false);
                    }
                });
                mpP.start();
            }
        } else {
            canvas.drawCircle((int) (iSpotX5 * correction), (int) (iSpotY5 * correction), radius, paint);
            ivb5.setImageResource(R.drawable.inventory_megamindy_cut);
        }
        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot6") == false) {
            if (x > iSpotX6 - margin && x < iSpotX6 + margin && y > iSpotY6 - margin && y < iSpotY6 + margin) {
                prefs.setSharedPreferences(PictureActivity.this, "pSpot6", true);
                prefs.setSharedPreferences(PictureActivity.this, "audio_playing", true);
                canvas.drawCircle((int) (iSpotX6 * correction), (int) (iSpotY6 * correction), radius, paint);
                mpP = MediaPlayer.create(PictureActivity.this, R.raw.gevonden_item_bal);
                mpP.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(PictureActivity.this, "audio_playing", false);
                    }
                });
                mpP.start();
                ivb6.setImageResource(R.drawable.inventory_bal_cut);
            }
        } else {
            canvas.drawCircle((int) (iSpotX6 * correction), (int) (iSpotY6 * correction), radius, paint);
            ivb6.setImageResource(R.drawable.inventory_bal_cut);
        }
        ivr.invalidate();

        if(prefs.returnSharedPreferences(PictureActivity.this, "pSpot1") &&
                prefs.returnSharedPreferences(PictureActivity.this, "pSpot2") &&
                prefs.returnSharedPreferences(PictureActivity.this, "pSpot3") &&
                prefs.returnSharedPreferences(PictureActivity.this, "pSpot4") &&
                prefs.returnSharedPreferences(PictureActivity.this, "pSpot5") &&
                prefs.returnSharedPreferences(PictureActivity.this, "pSpot6")) {

            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                if(!prefs.returnSharedPreferences(PictureActivity.this, "audio_playing")) {
                    if (checkPicture(event.getX(), event.getY())) {
                        prefs.setSharedPreferences(PictureActivity.this, "mov_mid", true);
                        btnGo.setVisibility(View.VISIBLE);
                        btnGo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                Intent intent = new Intent(PictureActivity.this, MovieActivity.class);
                                startActivity(intent);
                            }
                        });
                        btnGo.bringToFront();
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
            default:
                break;
        }
        return true;
    }
}