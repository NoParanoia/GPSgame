package com.noparanoia.gpsgame;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class GPSActivity extends Activity {

    Button btnDistanceTonext;
    Button btnGo;
    TextView txCurrentLocation;
    ImageView imgFoto;

    ImageButton ivb1;
    ImageButton ivb2;
    ImageButton ivb3;
    ImageButton ivb4;
    ImageButton ivb5;
    ImageButton ivb6;

    List<Location> locations = null;
    // GPSTracker class
    GPSTracker gps;
    MediaPlayer mpG;
    Preferences prefs;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //txCurrentLocation = findViewById(R.id.text_view);
        btnDistanceTonext = findViewById(R.id.distance_to_next_button);
        ivb1 = findViewById(R.id.bottom_image1);
        ivb2 = findViewById(R.id.bottom_image2);
        ivb3 = findViewById(R.id.bottom_image3);
        ivb4 = findViewById(R.id.bottom_image4);
        ivb5 = findViewById(R.id.bottom_image5);
        ivb6 = findViewById(R.id.bottom_image6);

        btnGo = findViewById(R.id.GO);
        btnGo.setVisibility(View.INVISIBLE);
        prefs = new Preferences();

        // Initialize shared preferences
        if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot1") == null) {
            prefs.setSharedPreferences(GPSActivity.this, "gSpot1", false);
        } else if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot1") == false) {
            ivb1.setImageResource(R.drawable.inventory_theepot_cut_gray);
        } else {
            ivb1.setImageResource(R.drawable.inventory_theepot_cut);
        }
        if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot2") == null) {
            prefs.setSharedPreferences(GPSActivity.this, "gSpot2", false);
        } else if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot2") == false) {
            ivb2.setImageResource(R.drawable.inventory_vis_cut_gray);
        } else {
            ivb2.setImageResource(R.drawable.inventory_vis_cut);
        }
        if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot3") == null) {
            prefs.setSharedPreferences(GPSActivity.this, "gSpot3", false);
        } else if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot3") == false) {
            ivb3.setImageResource(R.drawable.inventory_winkelwagen_cut_gray);
        } else {
            ivb3.setImageResource(R.drawable.inventory_winkelwagen_cut);
        }
        if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot4") == null) {
            prefs.setSharedPreferences(GPSActivity.this, "gSpot4", false);
        } else if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot4") == false) {
            ivb4.setImageResource(R.drawable.inventory_slang_cut_gray);
        } else {
            ivb4.setImageResource(R.drawable.inventory_slang_cut);
        }
        if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot5") == null) {
            prefs.setSharedPreferences(GPSActivity.this, "gSpot5", false);
        } else if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot5") == false) {
            ivb5.setImageResource(R.drawable.inventory_megamindy_cut_gray);
        } else {
            ivb5.setImageResource(R.drawable.inventory_megamindy_cut);
        }
        if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot6") == null) {
            prefs.setSharedPreferences(GPSActivity.this, "gSpot6", false);
        } else if(prefs.returnSharedPreferences(GPSActivity.this, "gSpot6") == false) {
            ivb6.setImageResource(R.drawable.inventory_bal_cut_gray);
        } else {
            ivb6.setImageResource(R.drawable.inventory_bal_cut);
        }

        ivb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!prefs.returnSharedPreferences(GPSActivity.this, "audio_playing")) {
                    if (checkDistance("F17", false)) {
                        prefs.setSharedPreferences(GPSActivity.this, "gSpot1", true);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        ivb1.setImageResource(R.drawable.inventory_theepot_cut);
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.goedzo_theepot);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    } else {
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.nietgoed_theepot);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    }
                    mpG.start();
                }
            }
        });

        ivb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!prefs.returnSharedPreferences(GPSActivity.this, "audio_playing")) {
                    if (checkDistance("F9", false)) {
                        prefs.setSharedPreferences(GPSActivity.this, "gSpot2", true);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.goedzo_vis);
                        ivb2.setImageResource(R.drawable.inventory_vis_cut);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    } else {
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.nietgoed_vis);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    }
                    mpG.start();
                }
            }
        });

        ivb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!prefs.returnSharedPreferences(GPSActivity.this, "audio_playing")) {
                    if (checkDistance("F21", false)) {
                        prefs.setSharedPreferences(GPSActivity.this, "gSpot3", true);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.goedzo_winkelwagen);
                        ivb3.setImageResource(R.drawable.inventory_winkelwagen_cut);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    } else {
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.nietgoed_winkelwagen);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    }
                    mpG.start();
                }
            }
        });

        ivb4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!prefs.returnSharedPreferences(GPSActivity.this, "audio_playing")) {
                    if (checkDistance("F4", false)) {
                        prefs.setSharedPreferences(GPSActivity.this, "gSpot4", true);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.goedzo_slang);
                        ivb4.setImageResource(R.drawable.inventory_slang_cut);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    } else {
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.nietgoed_winkelwagen);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    }
                    mpG.start();
                }
            }
        });

        ivb5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!prefs.returnSharedPreferences(GPSActivity.this, "audio_playing")) {
                    if (checkDistance("F22", false)) {
                        prefs.setSharedPreferences(GPSActivity.this, "gSpot5", true);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        prefs.setSharedPreferences(GPSActivity.this, "mov_end", true);
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.goedzo_megamindy);
                        ivb5.setImageResource(R.drawable.inventory_megamindy_cut);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                        prefs.setSharedPreferences(GPSActivity.this, "mov_end", true);
                        btnGo.setVisibility(View.VISIBLE);
                        btnGo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                Intent intent = new Intent(GPSActivity.this, MovieActivity.class);
                                startActivity(intent);
                            }
                        });
                        btnGo.bringToFront();
                    } else {
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.nietgoed_megamindy);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    }
                    mpG.start();
                }
            }
        });

        ivb6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!prefs.returnSharedPreferences(GPSActivity.this, "audio_playing")) {
                    if (checkDistance("F13", false)) {
                        prefs.setSharedPreferences(GPSActivity.this, "gSpot6", true);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.goedzo_bal);
                        ivb6.setImageResource(R.drawable.inventory_bal_cut);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    } else {
                        mpG = MediaPlayer.create(GPSActivity.this, R.raw.nietgoed_bal);
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                        mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                            }
                        });
                    }
                    mpG.start();
                }
            }
        });

        ParseXML();

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                getCoordinates();
                DrawImage();
                // restart every 5 seconds
                handler.postDelayed(this, 5000);
            }
        };
        // start with one second delay
        handler.postDelayed(runnable, 1000);

        // show location button click event
        btnDistanceTonext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                int next_loc = prefs.returnSharedInt(GPSActivity.this, "current_loc") + 1;

                if(next_loc >= 22) {
                    Toast.makeText(GPSActivity.this, "Einde", Toast.LENGTH_SHORT).show();
                } else {
                    String next = "F"+String.valueOf(next_loc);
                    checkDistance(next, true);
                }
            }
        });

    }

    public void getCoordinates() {
        gps = new GPSTracker(GPSActivity.this);
        StringBuilder sb = new StringBuilder();

        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            sb.append("Latitude = ").append(latitude).append("\n")
                    .append("Longitude = ").append(longitude).append("\n")
            ;
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
        //txCurrentLocation.setText(sb.toString());
    }

    boolean checkDistance(String dest, boolean message) {
        double lat = 0;
        double lng = 0;
        int rad = 0;
        for (Location location : locations) {
            if(location.getName().equals(dest)) {
                lat = location.getLatitude();
                lng = location.getLongitude();
                rad = location.getRadius();
            }
        }

        gps = new GPSTracker(GPSActivity.this);

        android.location.Location destination = new android.location.Location("Destination");
        destination.setLatitude(lat);
        destination.setLongitude(lng);

        // check if GPS enabled
        if (gps.canGetLocation()) {

            android.location.Location current = gps.getLocation();

            double distance = (current.distanceTo(destination));
            // \n is for new line
            if(message) {
                Toast.makeText(getApplicationContext(), "De afstand to het volgende punt is " + (int) distance + " meter", Toast.LENGTH_LONG).show();
                return false;
            } else {
                if(distance < rad) {
                    return true;
                }
            }

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
            return false;
        }
        return false;
    }

    private void ParseXML() {
        XmlPullParserFactory parserFactory;
        try {
            parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            InputStream is = null;
            FileInputStream fis = null;
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            // Check of er een settings.xml in de QYN map staat. Zo niet gebruikt hij de pre-compiled uit de assets folder
            if (!MainActivity.coordinatesOnDisk.exists()) {
                is = getAssets().open(MainActivity.coordinatesfileName);
                parser.setInput(is, null);
            } else {
                fis = new FileInputStream(MainActivity.coordinatesOnDisk);
                parser.setInput(fis, null);
            }
            processParsing(parser);

        } catch (XmlPullParserException e) {
        } catch (IOException e) {
        }
    }

    private void processParsing(XmlPullParser parser) throws IOException, XmlPullParserException {
        int eventType = parser.getEventType();
        Location currentLocation = null;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String tagName = null;

            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    locations = new ArrayList();
                    break;
                case XmlPullParser.START_TAG:
                    tagName = parser.getName();
                    if ("location".equals(tagName)) {
                        currentLocation = new Location();
                    } else if (currentLocation != null) {
                        if("name".equals(tagName)) {
                            currentLocation.setName(parser.nextText());
                        } else if ("latitude".equals(tagName)) {
                            currentLocation.setLatitude(Double.parseDouble(parser.nextText()));
                        } else if ("longitude".equals(tagName)) {
                            currentLocation.setLongitude(Double.parseDouble(parser.nextText()));
                        } else if ("radius".equals(tagName)) {
                            currentLocation.setRadius(Integer.parseInt(parser.nextText()));
                        }
                    }
                    break;
                case XmlPullParser.END_TAG:
                    tagName = parser.getName();
                    if("location".equals(tagName) && currentLocation != null) {
                        locations.add(currentLocation);
                    }
            }
            eventType = parser.next();
        }
    }

    public void DrawImage() {
        imgFoto = findViewById(R.id.foto_view);
        if (checkDistance("Work", false)) {
            prefs.setSharedInt(GPSActivity.this, "current_loc", 0);
            imgFoto.setImageResource(R.drawable.work);
        } else if (checkDistance("F1", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F1", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 1);
            imgFoto.setImageResource(R.drawable.f1);
        } else if (checkDistance("F2", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F2", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 2);
            imgFoto.setImageResource(R.drawable.f2);
        } else if (checkDistance("F3", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F3", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 3);
            imgFoto.setImageResource(R.drawable.f3);
        } else if (checkDistance("F4", false)) {
            imgFoto.setImageResource(R.drawable.f4);
            if(!prefs.returnSharedPreferences(GPSActivity.this, "F4")) {
                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                prefs.setSharedPreferences(GPSActivity.this, "F4", true);
                prefs.setSharedInt(GPSActivity.this, "current_loc", 4);
                mpG = MediaPlayer.create(GPSActivity.this, R.raw.zoekopdracht1);
                mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                    }
                });
                mpG.start();
            }

        } else if (checkDistance("F5", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F5", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 5);
            imgFoto.setImageResource(R.drawable.f5);
        } else if (checkDistance("F6", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F6", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 6);
            imgFoto.setImageResource(R.drawable.f6);
        } else if (checkDistance("F7", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F7", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 7);
            imgFoto.setImageResource(R.drawable.f7);
        } else if (checkDistance("F8", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F8", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 8);
            imgFoto.setImageResource(R.drawable.f8);
        } else if (checkDistance("F9", false)) {
            imgFoto.setImageResource(R.drawable.f9);
            if(!prefs.returnSharedPreferences(GPSActivity.this, "F9")) {
                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                prefs.setSharedPreferences(GPSActivity.this, "F9", true);
                prefs.setSharedInt(GPSActivity.this, "current_loc", 9);
                mpG = MediaPlayer.create(GPSActivity.this, R.raw.zoekopdracht2);
                mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                    }
                });
                mpG.start();
            }
        } else if (checkDistance("F10", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F10", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 10);
            imgFoto.setImageResource(R.drawable.f10);
        } else if (checkDistance("F11", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F11", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 11);
            imgFoto.setImageResource(R.drawable.f11);
        } else if (checkDistance("F12", false)) {
            imgFoto.setImageResource(R.drawable.f11);
            if(!prefs.returnSharedPreferences(GPSActivity.this, "F12")) {
                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                prefs.setSharedPreferences(GPSActivity.this, "F12", true);
                prefs.setSharedInt(GPSActivity.this, "current_loc", 12);
                mpG = MediaPlayer.create(GPSActivity.this, R.raw.hier_naar_links);
                mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                    }
                });
                mpG.start();
            }
        } else if (checkDistance("F13", false)) {
            imgFoto.setImageResource(R.drawable.f13);
            if(!prefs.returnSharedPreferences(GPSActivity.this, "F13")) {
                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                prefs.setSharedPreferences(GPSActivity.this, "F13", true);
                prefs.setSharedInt(GPSActivity.this, "current_loc", 13);
                mpG = MediaPlayer.create(GPSActivity.this, R.raw.zoekopdracht3);
                mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                    }
                });
                mpG.start();
            }
        } else if (checkDistance("F14", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F14", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 14);
            imgFoto.setImageResource(R.drawable.f14);
        } else if (checkDistance("F15", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F15", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 15);
            imgFoto.setImageResource(R.drawable.f15);
        } else if (checkDistance("F16", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F16", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 16);
            imgFoto.setImageResource(R.drawable.f16);
        } else if (checkDistance("F17", false)) {
            imgFoto.setImageResource(R.drawable.f17);
            if(!prefs.returnSharedPreferences(GPSActivity.this, "F17")) {
                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                prefs.setSharedPreferences(GPSActivity.this, "F17", true);
                prefs.setSharedInt(GPSActivity.this, "current_loc", 17);
                mpG = MediaPlayer.create(GPSActivity.this, R.raw.zoekopdracht4);
                mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                    }
                });
                mpG.start();
            }
        } else if (checkDistance("F18", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F18", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 18);
            imgFoto.setImageResource(R.drawable.f18);
        } else if (checkDistance("F19", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F19", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 19);
            imgFoto.setImageResource(R.drawable.f19);
        } else if (checkDistance("F20", false)) {
            prefs.setSharedPreferences(GPSActivity.this, "F20", true);
            prefs.setSharedInt(GPSActivity.this, "current_loc", 20);
            imgFoto.setImageResource(R.drawable.f20);
        } else if (checkDistance("F21", false)) {
            imgFoto.setImageResource(R.drawable.f21);
            if(!prefs.returnSharedPreferences(GPSActivity.this, "F21")) {
                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                prefs.setSharedPreferences(GPSActivity.this, "F21", true);
                prefs.setSharedInt(GPSActivity.this, "current_loc", 21);
                mpG = MediaPlayer.create(GPSActivity.this, R.raw.zoekopdracht5);
                mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                    }
                });
                mpG.start();
            }
        } else if (checkDistance("F22", false)) {
            imgFoto.setImageResource(R.drawable.f22);
            if(!prefs.returnSharedPreferences(GPSActivity.this, "F22")) {
                prefs.setSharedPreferences(GPSActivity.this, "audio_playing", true);
                prefs.setSharedPreferences(GPSActivity.this, "F22", true);
                prefs.setSharedInt(GPSActivity.this, "current_loc", 22);
                mpG = MediaPlayer.create(GPSActivity.this, R.raw.zoekopdracht6);
                mpG.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        prefs.setSharedPreferences(GPSActivity.this, "audio_playing", false);
                    }
                });
                mpG.start();
            }
        /*
        } else {
            imgFoto.setImageResource(R.drawable.nl);
        */
        }
    }
}
