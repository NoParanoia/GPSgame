package com.noparanoia.gpsgame;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {


    ImageButton btnSwitchToGPSActivity;
    ImageButton btnSwitchToMovieActivity;
    ImageButton btnSwitchToPictureActivity;
    ImageButton btnSettings;


    static String folderName = "NoParanoia";
    static String coordinatesfileName = "coordinates.xml";
    static String spotsfileName = "spots.xml";
    File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), folderName);
    static File coordinatesOnDisk = new File(Environment.getExternalStorageDirectory(), folderName + "/" + coordinatesfileName);
    static File spotsOnDisk = new File(Environment.getExternalStorageDirectory(), folderName + "/" + spotsfileName);

    // GPSTracker class
    GPSTracker gps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        btnSwitchToGPSActivity = findViewById(R.id.switch_to_gps_activity);
        btnSwitchToMovieActivity = findViewById(R.id.switch_to_movie_activity);
        btnSwitchToPictureActivity = findViewById(R.id.switch_to_picture_activity);
        btnSettings = findViewById(R.id.settings);


        btnSwitchToGPSActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Start your second activity
                Intent intent = new Intent(MainActivity.this, GPSActivity.class);
                startActivity(intent);
            }
        });

        btnSwitchToMovieActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Start your second activity
                Intent intent = new Intent(MainActivity.this, MovieActivity.class);
                startActivity(intent);
            }
        });

        btnSwitchToPictureActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Start your second activity
                Intent intent = new Intent(MainActivity.this, PictureActivity.class);
                startActivity(intent);
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                SharedPreferences.Editor editor = getSharedPreferences("prefs", MODE_PRIVATE).edit();
                                editor.clear().commit();
                                editor.apply();
                                Toast.makeText(getApplicationContext(), "-done-", Toast.LENGTH_LONG).show();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Hiermee reset je het spel, weet je zeker dat je alle behaalde resultaten wilt wissen?").setPositiveButton("Ja", dialogClickListener)
                        .setNegativeButton("Nee", dialogClickListener).show();



            }
        });
    }
}

